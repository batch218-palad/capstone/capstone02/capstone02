const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "Product is required"]
	},
	description:{
		type: String,
		required: [true, "Desciption is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	quantity:{
		type: Number,
		required: [true, "Quantity is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	buyers: [
			{
				userId:{
					type: String,
					required: [true, "UserId is required"]
				},
				orderedOn:{
					type: Date,
					default: new Date()
				}

			}
		]
	});

module.exports = mongoose.model("Product", productSchema);