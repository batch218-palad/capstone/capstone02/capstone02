const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoute = require("./routes/userRoute.js")
const productRoute = require("./routes/productRoute.js")


const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoute);
app.use("/products", productRoute);


mongoose.connect("mongodb+srv://admin:admin@batch218capstone02.tbiqr7g.mongodb.net/Capstone02?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


mongoose.connection.once('open', () => console.log('Now connected to Palad-Mongo DB Atlas.'));


app.listen(process.env.PORT || 8000, () => 
	{console.log(`API is now online on port ${process.env.PORT || 8000 }`)
});
