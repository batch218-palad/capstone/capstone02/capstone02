const express = require("express"); 
const router = express.Router(); 
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

//---checkItemExist
router.post("/checkItem", (req, res) =>{
	productController.checkItemExist(req.body).then(resultFromController => res.send(resultFromController))
});

/*router.get("/checkItem", auth.verify, (req, res) => {
	const productData = auth.decode(req.headers.authorization);
		console.log(productData)
		console.log(req.headers.authorization);
	productController.getName({name: productData.name}).then(resultFromController => res.send(resultFromController))
});*/
//---

router.post("/create",  auth.verify, (req,res) =>{
	const newData = {
		product: req.body,      //headers
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(newData).then(resultFromController => res.send(resultFromController))
});


router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
});
//--

router.get("/all", (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController))
});


/*router.get("/all", (req, res) => {
    productController.getAllProduct().then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});*/
//--
router.get("/:productId", (req, res) => {
									//retrieves the id from the url
	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
});


router.patch("/:productId/update", auth.verify, (req, res) => 
{
	const newData = {
		product: req.body,      //headers
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params.productId, newData).then(resultFromController => {
		res.send(resultFromController)
	});
});


router.patch("/:productId/archive", auth.verify, (req, res) => 
{
	const newStatus = {
		product: req.body,      //headers
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(req.params.productId, newStatus).then(resultFromController => {
		res.send(resultFromController)
	});
});

router.patch("/:productId/activate", auth.verify, (req, res) => 
{
	const newStatus = {
		product: req.body,      //headers
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.activateProduct(req.params.productId, newStatus).then(resultFromController => {
		res.send(resultFromController)
	});
});








 

/*router.get('/all',auth.verify,auth.verifyAdmin, productController.getAllProducts)*/










module.exports = router;















