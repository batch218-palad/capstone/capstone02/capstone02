
const express = require("express");

const router = express.Router();
const User = require("../models/user.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/order", auth.verify, (req, res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	userController.order(data).then(resultFromController => res.send(resultFromController));

});

//---

router.get("/userOrders", (req, res) => {
	userController.getUserOrders(req.body).then(resultFromController => res.send(resultFromController))
});

//---

router.post("/cart", auth.verify, (req, res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	userController.cart(data).then(resultFromController => res.send(resultFromController));

});
  



router.post("/userDetails", (req, res) =>{
	userController.getUser(req.body).then(resultFromController => res.send(resultFromController))
});
//-----
router.get("/userDetails", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});
//-----
//------

router.get("/allUser", (req, res) => {
	userController.getAllUser().then(resultFromController => res.send(resultFromController))
});

//------

// router.get("/:userId/Details", (req, res) =>{
// 	userController.getOneUser(req.body).then(resultFromController => res.send(resultFromController))
// });




router.patch("/:userId/updateUser", auth.verify, (req, res) => 
{
	const newData = {
		user: req.body,      //headers
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.updateUser(req.params.userId, newData).then(resultFromController => {
		res.send(resultFromController)
	});
});

module.exports = router;





