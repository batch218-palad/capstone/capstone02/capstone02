const jwt = require("jsonwebtoken");
const secret = "ECommerceAPI";

 
module.exports.createAccessToken = (user) =>{
	// payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin

	}
									// {} is callbank function - can put expiration
	return jwt.sign(data, secret, {}); // expiresIn : "60s"
}

// to verify a token from the e=request (from postman)
module.exports.verify = (request, response, next) => {

	// get JWT (JSON web Token) from postman
	let token = request.headers.authorization

	if(typeof token !== "undefined"){
		console.log(token);

		// remove 1st 7 characters ("Bearer ") from the token
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) =>{
			if (error){
				return response.send({
					auth: "Failed."
				})
			}
			else{
				next()
			}
		})
	}
	else{
		return null
	}
}


// to decode the user details from the token
module.exports.decode = (token) =>{

	if(typeof token !== "undefined"){

		// remove 1st 7 characters ("Bearer ") from the token
		token = token.slice(7, token.length);
	}

	return jwt.verify(token, secret, (error, data) => {
		if(error){
			return null
		}
		else{
			return jwt.decode(token, {complete:true}).payload
		}
	})
}