const User = require("../models/user.js");
const Product = require("../models/product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
		isAdmin: reqBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
}

//----
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};
//-----
module.exports.order = async (data) => {
	if(data.isAdmin == true){
		return false;
		} else {
			
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.orders.push({productId : data.productId});
			return user.save().then((user,error) => {
				if(error) {
					return false;
				} else {
					return true;
				}
			})
		})
	
		let isProductUpdated = await Product.findById(data.productId).then(product => {
			product.buyers.push({userId : data.userId});
			return product.save().then((product, error) =>{
				if (error){
					return false;
				} else {
					return true;
				}
			})
		})


		if(isUserUpdated && isProductUpdated){
			return true;
		} else {
			return false
		}
		}
	};

//-----
module.exports.getUserOrders = (userId) => {
	return User.find(userId.orders).then((spcfcUser, err)=>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			spcfcUser.password = "";
			return spcfcUser;
		}
	})
};

//--------


module.exports.cart = async (data) => {
	if(data.isAdmin == true){
		return (`User is not authorized to add an item to a cart`);
		} else {
			
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.cart.push({productId : data.productId});
			return user.save().then((user,error) => {
				if(error) {
					return false;
				} else {
					return true;
				}
			})
		})
	
		let isProductUpdated = await Product.findById(data.productId).then(product => {
			product.buyers.push({userId : data.userId});
			return product.save().then((product, error) =>{
				if (error){
					return false;
				} else {
					return true;
				}
			})
		})


		if(isUserUpdated && isProductUpdated){
			return (`Item added to cart.`);
		} else {
			return (`Unable to add to your cart.`)
		}
		}
	};



module.exports.getUser = (userId) => {
	return User.findById(userId._id).then((spcfcUser, err)=>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			spcfcUser.password = "";
			return spcfcUser;
		}
	})
};



module.exports.updateUser = (userId, newData) =>{
	if(newData.isAdmin == true){

		//update code
		return User.findByIdAndUpdate(userId, 
			{
				isAdmin: newData.user.isAdmin
			}
		).then((updateUser, error)=>{
			if(error){
				return `Cannot update user.`
			}
			return `User promoted as an admin.`
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value)=> {return value})
	}
}



module.exports.getAllUser = () => {
	return User.find({}).then(result =>{
		return result
	})
}


// module.exports.getOneUser = (userId) =>{
// 	return User.findById(userId._id).then(result =>{
// 		return result
// 	})
// }



