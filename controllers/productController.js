const mongoose = require("mongoose");
const Product = require("../models/product.js");

//----
module.exports.checkItemExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a field value from a collection
	return Product.find({name:reqBody.name}).then(result => {
		// condition if there is an existing user
		if (result.length > 0){
			return true;
		}
		// condition if there is no existing user
		else
		{
			return false;
		}
	})
}

/*module.exports.getName = (productData) => {
	return Product.find(productData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};*/
//----

module.exports.addProduct = (newData) => {
if(newData.isAdmin == true){

	let newProduct = new Product ({
		name: newData.product.name,
		description: newData.product.description,
		price: newData.product.price,
		quantity: newData.product.quantity
	})

	return newProduct.save().then((newProduct, error) =>{
		if(error){
			return error;
		}
		else{
			return newProduct;
		}
	})
	}
else{
	let message = Promise.resolve('User must be ADMIN to access this');
	return message.then((value)=> {return value})
	}
}


module.exports.getActiveProducts = () =>{
	return Product.find({isActive:true}).then(result =>{
		return result
	})
};


module.exports.getProduct = (productId) =>{
						// inside the parenthesis should be the id
	return Product.findById(productId).then(result =>{
		return result
	})
};


module.exports.updateProduct = (productId, newData) =>{
	if(newData.isAdmin == true){

		//update code
		return Product.findByIdAndUpdate(productId, 
			{
				name: newData.product.name,
				description: newData.product.description,
				price: newData.product.price,
				quantity: newData.product.quantity
			}
		).then((updateProduct, error)=>{
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value)=> {return value})
	}
}


module.exports.archiveProduct = (productId, newStatus) =>{
	if(newStatus.isAdmin == true){

		//update code
		return Product.findByIdAndUpdate(productId, 
			{
				isActive: newStatus.product.isActive,
			}
		).then((archiveProduct, error)=>{
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value)=> {return value})
	}
}

//--

module.exports.activateProduct = (productId, newStatus) =>{
	if(newStatus.isAdmin == true){

		//update code
		return Product.findByIdAndUpdate(productId, 
			{
				isActive: newStatus.product.isActive,
			}
		).then((activateProduct, error)=>{
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value)=> {return value})
	}
}
//--



module.exports.getAllProduct = () => {
	return Product.find({}).then(result =>{
		return result
	})
}



/*module.exports.getAllProduct = () => Product.find({}).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Failed to retrieve products at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            
           
            products: result,
        },
    };
});*/




/*module.exports.getAllProduct = (req, res) => {
    return Product.find({}).then(result => {
        return res.send(result);
    })
}*/








